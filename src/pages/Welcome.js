import React from 'react';
import {Image, StyleSheet, Text, View, TextInput, TouchableOpacity} from 'react-native';
import {Logo} from '../assets';

const Welcome = () => {
    return (
        <View style={styles.wrapper}>
            <Image style={styles.logo} source={Logo} />
            <Text style={styles.logoText}>Welcome to My App</Text>

            <TextInput style={styles.inputBox} underlineColorAndroid='black'
            placeholder='Email' placeholderTextColor='silver' keyboardType='email-address' />

            <TextInput style={styles.inputBox} underlineColorAndroid='black'
            placeholder='Password' placeholderTextColor='silver' secureTextEntry={true} />

            <TouchableOpacity style={styles.button}>
                <Text style={styles.buttonText}>Login</Text>
            </TouchableOpacity>

            <View style={styles.container}>
                <Text style={styles.signupText}>Don't have an account yet?</Text>
                <TouchableOpacity>
                    <Text style={styles.signupButton}> Signup</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default Welcome;

const styles = StyleSheet.create({
    wrapper : {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    logo: {
        width: 190,
        height: 70
    },
    logoText : {
        marginVertical: 15,
        fontSize: 16,
        color: 'black',
        fontWeight: 'bold'
    },
    inputBox: {
        width: 300,
        paddingHorizontal: 16,
        fontSize: 16,
        color: 'black',
        marginVertical: 10
    },
    button: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13
    },
    buttonText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: 'white',
        textAlign: 'center'
    },
    container : {
        justifyContent : 'center',
        paddingVertical: 16,
        flexDirection: 'row'
    },
    signupText: {
        color: 'black',
        fontSize: 16
    },
    signupButton: {
        color: 'black',
        fontSize: 16,
        fontWeight: 'bold'
    }
});