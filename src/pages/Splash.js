import React, {useEffect} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {Logo} from '../assets';

const Splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Welcome');
        }, 3000);
    });
    return (
        <View style={styles.wrapper}>
            <Image source={Logo} style={styles.logo} />
            <Text style={styles.loadText}>Loading...</Text>
        </View>
    );
};

export default Splash;

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        margin: 10,
        width: 200,
        height: 72
    },
    loadText: {
        fontSize: 18,
        fontWeight: 'bold'
    }
});