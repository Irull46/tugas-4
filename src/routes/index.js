import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {Splash, Welcome} from '../pages';

const Stack = createStackNavigator();

const Route = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="Splash"
                component={Splash}
                options={{headerShown: false}}
            />
            <Stack.Screen
                name="Welcome"
                component={Welcome}
                options={{headerShown: false}}
            />
        </Stack.Navigator>
    );
};

export default Route;